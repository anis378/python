import numpy as np
def simp13(f,a,b,N):

    if N % 2 == 1:
        raise ValueError("N must be an even integer.")
    
    x = np.linspace(a,b,N+1)
    print(x)
    y = f(x)
    a1=y[0:-1:2]
    b1=y[2::2]
    m1=y[1::2]
    h = (b-a)/N
    S = h/3 * np.sum(a1+b1+ 4*m1)
    print("Area= ",S)
    
simp13(lambda x : 3*x**2,0,1,10)
