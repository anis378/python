import numpy as np
def simp13(f,a,b,N):
    
    x = np.linspace(a,b,N+1)
    print(x)
    a1=f(a)
    b1=f(b)
    m1=f((2*a+b)/3)
    m2=f((2*b+a)/3)

    h = (b-a)/N
    S =( (3*h/8) * (a1+b1+3*m1+3*m2))
    print("Area= ",S)
    
simp13(lambda x : 3*x**2,0,1,10)
