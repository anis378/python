import numpy as np
def trap(f,a,b,N):
   
    x = np.linspace(a,b,N+1)
    print(x)
    y = f(x)
    b1 = y[1:]
    a1 = y[:-1]
    h = (b - a)/N
    T = (h/2) * np.sum(a1 + b1)
    print("Area= ",T)

trap(np.sin,0,np.pi/2,6)
#trapz(lambda x : 3*x**2,0,1,10000)