import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt
f= open("poly.txt")
ar=[]
for t in f:
    ar.append([int(i) for i in t.split()])
    #print(ar)
ar=np.array(ar)
x = ar[:,0]
y = ar[:,1]
'''
print("Values of X= "x)
print("values of y= "y)
'''
n=len(x)
sumx=sum(x)
sumy=sum(y)
sumx2=sum(x**2)
sumx3=sum(x**3)
sumx4=sum(x**4)
sumxy=sum(x*y)
sumx2y=sum(x**2*y)
a=[[n,sumx,sumx2],[sumx,sumx2,sumx3],[sumx2,sumx3,sumx4]]
b=[sumy,sumxy,sumx2y]
sol=npl.solve(a,b)
nx=[i for i in np.arange(0,4,0.1)]
py=[sol[0]+i*sol[1]+(i**2)*sol[2] for i in np.arange(0,4,0.1)]
plt.plot(nx,py)
plt.scatter(x,y,color='r')
plt.show()
